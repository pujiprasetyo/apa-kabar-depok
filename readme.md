# Apa Kabar Depok

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Website ini merupakan tugas akhir dari praktikum materi pemrogram web. Pada web ini menggunakan beberapa framework dan tools antara lain:

  - CodeIgniter
  - Materialize
  - Jquery
  - Database MySql

# Features!

  - Halaman awal (home) untuk berita terupdate
  - Halaman popular mmerupakan kumpulan berita  yang paling banyak dilihat
  - Halaman login bagi para user yang ingin berkontribusi memberikan info seputar tentang apa yang terjadi di daerah depok
