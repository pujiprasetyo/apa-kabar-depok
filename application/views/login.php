<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container" style="padding: 15px">
	<div class="card-panel z-depth-3">
		<form action="<?= site_url('auth/login')?>" method="post">
			<h5>Username</h5>
				<input id="username" name="username" type="text" placeholder="Enter your username" required autofocus/>
			<h5>Password</h5>
				<input id="password" name="password" type="password" placeholder="Enter your password" required/>
			<center>
				<button name="login" class="center btn waves-effect grey">Login</button>
			</center>
		</form>
		<!--
		<p>Do you haven't account ?</p>
		<button name="signUp" onclick="alert('Sorry under maintenance ...')" class="btn waves-effect grey">Sign Up</button>
		-->
	</div>
</div>