<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

</main>
<footer class="page-footer grey darken-3">
	<div class="container">
		<div class="row">
			<div class="col l4 s12" style="border:1">
				<p class="grey-text text-lighten-4">
				Portal berita <i>Apa Kabar Depok</i> merupakan layanan yang memberikan informasi yang terupdate, terhangat serta terpercaya.<br>
				Kurang lebih seperti itu.
				</p>
			</div>
			<div class="col l3 offset-l2 s12">
				<h5 class="white-text">Links</h5>
				<ul>
					<li><a href="<?= site_url('blog'); ?>" class="grey-text text-lighten-3">Home</a></li>
					<li><a href="<?= site_url('blog/about'); ?>" class="grey-text text-lighten-3">About</a></li>
					<li><a href="<?= site_url('blog/contact'); ?>" class="grey-text text-lighten-3">Contact</a></li>
				</ul>
			</div>
			<div class="col l3 s12">
				<h5 class="white-text">Follow Us</h5>
				<ul>
					<a href="#" title="Ikuti Apa Kabar Depok di Facebook"><img src="<?= site_url('asset/images/icon/facebook.png'); ?>" width="25" height="25" style="margin-right: 10px"></a>
					<a href="#" title="Ikuti Apa Kabar Depok di Twitter"><img src="<?= site_url('asset/images/icon/twitter.png'); ?>" width="25" height="25" style="margin-right: 10px"></a>
					<a href="#" title="Ikuti Apa Kabar Depok di Instagram"><img src="<?= site_url('asset/images/icon/instagram.png'); ?>" width="25" height="25" style="margin-right: 10px"></a>
					<a href="#" title="Ikuti Apa Kabar Depok di Youtube"><img src="<?= site_url('asset/images/icon/youtube.png'); ?>" width="25" height="25" style="margin-right: 10px"></a>
					<a href="#" title="Ikuti Apa Kabar Depok di Google+"><img src="<?= site_url('asset/images/icon/google-plus.png'); ?>" width="25" height="25"></a>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright grey darken-4">
		<div class="container">
		<a class="grey-text text-lighten-4 left">&copy; 2017 Labti</a>
		<a class="grey-text text-lighten-4 right">4IA16</a>
		</div>
	</div>
</footer>

<style>
	body {
		display: flex;
		min-height: 100vh;
		flex-direction: column;
	}
	main {
		flex: 1 0 auto;
	}
</style>

<script src="<?= site_url('asset/js/jquery.min.js'); ?>"></script>
<script src="<?= site_url('asset/js/materialize.min.js'); ?>"></script>
<script>
	$(document).ready(function(){
		$('.button-collapse').sideNav();
	});
	
	/*
	function myLogin() {
		var username = document.getElementById('username').value;
		var password = document.getElementById("password").value;
		if (username.length != 0 && password.length != 0) {
			if (username == "admin" && password == "admin") {
				alert("Success (^_^)");
			} else {
				alert("Failed (-_-)\nYour not logged in");
			}
		}
	}
	*/
</script>

</body>
</html>