<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<?php if ($this->uri->segment(2) == "popular"): ?>
		<?php foreach ($berita as $news): ?>
			<div class="card-panel z-depth-2">
				<h5>
					<a href="<?= site_url('blog/berita/'.$news['link']); ?>" style="color: black"><?= $news['judul'];?></a>
				</h5>
				<hr>
				<p style="font-size: 13px">
					Diposting oleh : <?= $news['penulis'];?> pada <i><?= $news['tanggal'];?></i><br>
					Dilihat <?= $news['visitor'];?> kali
				</p>
			</div>
		<?php endforeach; ?>
		<center>
		<?php echo $this->pagination->create_links(); ?>
		</center>
	<?php else: ?>
		<?php foreach ($berita as $news): ?>
			<div class="card-panel z-depth-2">
				<h5>
					<a href="<?= site_url('blog/berita/'.$news['link']); ?>" style="color: black"><?= $news['judul'];?></a>
				</h5>
				<hr>
				<p style="font-size: 13px">
					Diposting oleh : <?= $news['penulis'];?> pada <i><?= $news['tanggal'];?></i>
				</p>
			</div>
		<?php endforeach; ?>
		<center>
		<?php echo $this->pagination->create_links(); ?>
		</center>
	<?php endif; ?>
</div>