<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<div class="left">
		<button onclick="window.history.go(-1)" type="submit" class="btn waves-effect grey">Back</button>
	</div>
	<form action="<?= site_url('admin/update/'.$berita->link)?>" method="post">
		<div style="padding: 0 0 50px 0">
			<div class="right">
				<button type="submit" class="btn waves-effect grey">Update</button>
			</div>
		</div>
		
		<div class="card-panel z-depth-3">
			<h5>Title</h5>
			<input id="judul" name="judul" type="text" value="<?= $berita->judul; ?>" placeholder="Insert news title" required/>
			<h5>Content</h5>
			<textarea id="konten" name="konten" class="materialize-textarea" placeholder="Write news content here" required><?= $berita->konten; ?></textarea>
		</div>
	</form>
</div>