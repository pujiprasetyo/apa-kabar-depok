<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Apa Kabar Depok</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width ,initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?= base_url('asset/css/materialize.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= site_url('asset/fonts/material-icons/material-icons.css'); ?>">
</head>

<body class="grey lighten-4">
	<div class="navbar-fixed">
		<nav class="orange darken-2">
		<a href="#" data-activates="mobile-menu" class="button-collapse">
			<i class="material-icons">menu</i>
		</a>
			<div class="container nav-wrapper ">
				<a href="<?= site_url(''); ?>" class="brand-logo">Apa Kabar Depok</a>
				<ul class="right hide-on-med-and-down">
					<li><a href="<?= site_url(''); ?>"><i class="material-icons left">home</i>Home</a></li>
					<?php if (!$this->session->has_userdata('username')):?>
						<li><a href="<?= site_url('blog/popular'); ?>" ><i class="material-icons left">grade</i>Popular</a></li>
						<li><a href="<?= site_url('auth/login'); ?>"><i class="material-icons left">account_box</i>Login</a></li>
					<?php else:?>
						<li><a href="<?= site_url('admin'); ?>"><i class="material-icons left">grade</i>Dashboard</a></li>
						<li><a href="<?= site_url('admin/logout'); ?>"><i class="material-icons left">account_box</i>Logout</a></li>
					<?php endif;?>
				</ul>
			</div>
		</nav>
	</div>
	<ul class="side-nav grey lighten-4" id="mobile-menu">
		<div class="center z-depth-3 orange darken-2" style="padding: 10px">
			<h5>Apa Kabar Depok</h5>
		</div>
		<li><a href="<?= site_url(''); ?>"><i class="material-icons">home</i>Home</a></li>
		<?php if (!$this->session->has_userdata('username')):?>
			<li><a href="<?= site_url('blog/popular'); ?>"><i class="material-icons">grade</i>Popular</a></li>
			<li><a href="<?= site_url('auth/login'); ?>"><i class="material-icons">account_box</i>Login</a></li>
		<?php else:?>
			<li><a href="<?= site_url('admin'); ?>"><i class="material-icons">grade</i>Dashboard</a></li>
			<li><a href="<?= site_url('admin/logout'); ?>"><i class="material-icons">account_box</i>Logout</a></li>
		<?php endif;?>
		<br>
		<li><a href="<?= site_url('blog/about'); ?>"><i class="material-icons">info</i>About</a></li>
		<li><a href="<?= site_url('blog/contact'); ?>"><i class="material-icons">phone</i>Contact</a></li>	
	</ul>
<main class="container">