<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<div class="left">
		<button onclick="window.history.go(-1)" type="submit" class="btn waves-effect grey">Back</button>
	</div>
	<?php echo form_open('admin/post'); ?>
		<div style="padding: 0 0 50px 0">
			<div class="right" >
				<button type="submit" class="btn waves-effect grey">Post</button>
			</div>
		</div>
		<div class="card-panel z-depth-3">
			<h5>Title</h5>
			<?php echo form_error('judul'); ?>
			<input id="judul" name="judul" type="text" placeholder="Insert news title"/>
			<h5>Content</h5>
			<?php echo form_error('konten'); ?>
			<textarea id="konten" name="konten" class="materialize-textarea" placeholder="Write news content here"></textarea>
		</div>
	<?php echo form_close(); ?>
</div>