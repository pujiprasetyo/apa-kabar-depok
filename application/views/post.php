<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<?php if ($this->session->has_userdata('username')):?>
		<div style="padding: 0 0 50px 0">
			<div class="left">
				<button onclick="window.history.go(-1)" type="submit" class="btn waves-effect grey">Back</button>
			</div>
		</div>
	<?php endif;?>
		<div class="card-panel z-depth-3">
			<h3 style="text-align: center"><?= $berita->judul; ?></h3><br>
			<p style="text-align: justify"><?= $berita->konten; ?></p><br>
			<p style="font-size: 12px">
				<i><?= $berita->tanggal; ?></i><br>
				<?= $berita->penulis; ?>
			</p>
		</div>
</div>
