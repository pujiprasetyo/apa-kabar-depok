<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="padding: 20px 0 20px 0">
	<p>
		<h5 class="center">Dashboard Admin</h5>
	</p>
	<p>
		<h5 class="right">Author : <?= $penulis->username; ?></h5>
	</p>
		<a href="<?= site_url('admin/post'); ?>" class="btn waves-effect grey">New Post</a>
	<br>
	
	<table class="striped mdl-data-table">
		<tr>
			<th>Date</th>
			<th>Title</th>
			<th colspan="3">Action</th>
		</tr>
		<?php foreach ($berita as $news): ?>
			<tr class="container">
				<td style="font-size: 12px"><i><?= $news['tanggal'];?></i></td>
				<td><?= $news['judul'];?></td>
				<td><a title="View" href="<?= site_url('admin/view/'.$news['link']);?>"><i class="material-icons">visibility</i></a></td>
				<td><a title="Edit" href="<?= site_url('admin/update/'.$news['link']);?>"><i class="material-icons">edit</i></a></td>
				<td><a title="Delete" href="<?= site_url('admin/delete/'.$news['link']);?>"><i class="material-icons">delete_forever</i></a></td>
			</tr>
		<?php endforeach; ?>
	</table>
	<center>
	<?php echo $this->pagination->create_links(); ?>
	</center>
</div>