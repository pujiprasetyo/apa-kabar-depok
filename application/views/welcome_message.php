<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Bikin Web</title>
	<link rel="stylesheet" type="text/css" href="<?=site_url('asset/css/materialize.min.css'); ?>">
</head>
<body>
	<style media="screen">
		h1{
			color: red;
		}
		.hijau{
			color: green;
		}
		.besar{ /*Dipake dapat multivalue u/ class*/
			font-size: 70px;
		}
		#biru{
			color: blue;
		}
		#ukuran{ /*Dipake hanya dapat satu value u/ id*/
			font-size: 70px;
		}
	</style>
	<center>
		<h1>Pertama</h1>
		<h1 class="hijau besar">Kedua</h1>
		<p id="biru">Ketiga</p>
		<p id="ukuran">Keempat</p>
		<div class="red white-text">
			Ini class pake materialize
		</div>
	</center>
</body>
</html>