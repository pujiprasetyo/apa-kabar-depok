<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class News_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	//Kumpulan function untuk Pembaca
	function readHome($link = FALSE) { //select
        if ($link == TRUE) {
			$query = $this->db->get_where('berita', array('link' => $link));
			$jml = $query->row('visitor');
			$jml = $jml + 1;
			$data = array('visitor' => $jml);
			$this->db->where('link', $link);
			$this->db->update('berita', $data);
			
			$query = $this->db->get_where('berita', array('link' => $link));
			return $query->row();
        }
	}
	
	function dataHome($number, $offset) {
		$this->db->order_by('id desc');
		$query = $this->db->get('berita', $number, $offset);
		return $query->result_array();
	}
	
	function dataPopular($number, $offset) {
		$this->db->order_by('visitor desc');
		$this->db->order_by('id desc');
		$query = $this->db->get('berita', $number, $offset);
		return $query->result_array();
	}
	
	function jumlah_data(){
		return $this->db->get('berita')->num_rows();
	}
	
	//Kumpulan function untuk Penulis
	public function post() { //insert
		$tanggal = date("l, jS F Y");
		$link = url_title($this->input->post('judul'), 'dash', TRUE);
		$namaSession = $this->session->userdata('username');
		$query = $this->db->get_where('super_user', array('username' => $namaSession));
		$namaPenulis = $query->row('username');

		$data = array(
			'tanggal' => $tanggal,
			'judul' => $this->input->post('judul'),
			'konten' =>  $this->input->post('konten'),
			'penulis' => $namaPenulis,
			'link' => $link
		);
		return $this->db->insert('berita', $data);
	}
	
	function read($link = FALSE) { //select
        if ($link == TRUE) {
			$query = $this->db->get_where('berita', array('link' => $link));
			return $query->row();
        }
	}
	
	function dataDashboard($number, $offset) {
		$this->db->order_by('id desc');
		$this->db->where('penulis', $this->session->userdata('username'));
		$query = $this->db->get('berita', $number, $offset);
		return $query->result_array();
	}
	
	public function update($link) { //update
		$linkBaru = url_title($this->input->post('judul'), 'dash', TRUE);

		$data = array(
			'judul' => $this->input->post('judul'),
			'konten' => $this->input->post('konten'),
			'link' => $linkBaru
		);
		$this->db->where('link', $link);
		return $this->db->update('berita', $data);
	}
	
	public function delete($link){ //delete
		$this->db->where('link', $link);
		return $this->db->delete('berita');
	}
}