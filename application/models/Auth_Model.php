<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_Model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	function cekPassword() {
		$this->db->select('password');
		$this->db->where('username', $this->input->post('username', true));
		$query = $this->db->get('super_user');
		return $query->row();
	}
	
	function readUser() {
		$query = $this->db->get_where('super_user', array('username' => $this->session->userdata('username')));
		return $query->row();
	}
}