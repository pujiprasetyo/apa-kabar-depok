<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->model('News_Model','news');
	}
	
	function index(){
		$alamat = 'blog/index';
		$modelAlamat = 'dataHome';
		$this->nomorHalaman($alamat, $modelAlamat);
	}
	
	function popular() {
		$alamat = 'blog/popular';
		$modelAlamat = 'dataPopular';
		$this->nomorHalaman($alamat, $modelAlamat);
	}
	
	function nomorHalaman($alamat, $modelAlamat) {
		$jumlah_data = $this->news->jumlah_data();
		$config['base_url'] = site_url($alamat);
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '<i class="material-icons">chevron_right</i>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '<i class="large material-icons">chevron_left</i>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active orange darken-2"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		$data['berita'] = $this->news->$modelAlamat($config['per_page'], $from);
		
		$this->load->view('header');
		$this->load->view('main', $data);
		$this->load->view('footer');
	}
	
	function berita($link = FALSE) {
		if ($link == TRUE) {
			$data['berita'] = $this->news->readHome($link);

			$this->load->view('header');
			$this->load->view('post', $data);
			$this->load->view('footer');
		}
	}
	
	function about() {
		$this->load->view('header');
		$this->load->view('about');
		$this->load->view('footer');
	}
	
	function contact() {
		$this->load->view('header');
		$this->load->view('contact');
		$this->load->view('footer');
	}
}