<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {
		parent:: __construct();
		$this->load->model('News_Model','news');
		$this->load->model('Auth_Model','auth');
		if (!$this->session->has_userdata('username')) {
			redirect('auth/login');
		}
	}
	
	function index() {
		$alamat = 'admin/index';
		$modelAlamat = 'dataDashboard';
		$this->nomorHalaman($alamat, $modelAlamat);
	}
	
	function nomorHalaman($alamat, $modelAlamat) {
		$jumlah_data = $this->news->jumlah_data();
		$config['base_url'] = site_url($alamat);
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 10;
		
		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '<i class="material-icons">chevron_right</i>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '<i class="large material-icons">chevron_left</i>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active orange darken-2"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		$data['berita'] = $this->news->$modelAlamat($config['per_page'], $from);
		$data['penulis'] = $this->auth->readUser();
		
		$this->load->view('header');
		$this->load->view('dashboard', $data);
		$this->load->view('footer');
	}
	
	function view($link = FALSE) {
		if ($link == TRUE) {
			$data['berita'] = $this->news->read($link);

			$this->load->view('header');
			$this->load->view('post', $data);
			$this->load->view('footer');
		}
	}
	
	function post() { //insert
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>'); // ini untuk bootstrap
		$this->form_validation->set_rules('judul','Title','required|is_unique[berita.judul]');
		$this->form_validation->set_rules('konten','Content','required');

		if($this->form_validation->run() == FALSE) {
			$this->load->view('header');
			$this->load->view('news');
			$this->load->view('footer');
		} else {
			$this->news->post();
			redirect('admin');
		}
	}
	
	function update($link){ //update
		$this->form_validation->set_rules('judul','Title','required');
		$this->form_validation->set_rules('konten','Content','required');

		if($this->form_validation->run() == FALSE) {
			$data['berita']=$this->news->read($link);

			$this->load->view('header');
			$this->load->view('news_update', $data);
			$this->load->view('footer');
		} else {
			$this->news->update($link);
			redirect("admin");
		}
	}
	
	function delete($link){ //delete
		$this->news->delete($link);
		redirect('admin');
	}
	
	function logout() {
		if ($this->session->has_userdata('username')) {
			$this->session->sess_destroy();
			redirect('blog');
		} else {
			redirect('admin');
		}
	}
}