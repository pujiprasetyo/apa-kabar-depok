<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->model('Auth_Model', 'auth');
		if ($this->session->has_userdata('username')) {
			redirect('admin');
		} else if ($this->uri->segment(2) != 'login') {
			redirect('auth/login');
		}
	}
	
	function login() {
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|callback_passwordCheck');
		
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('header');
			$this->load->view('login');
			$this->load->view('footer');
		} else {
			$this->session->set_userdata('username', $this->input->post('username', TRUE));
			redirect('admin');
		}
	}
	
	function passwordCheck() {
		$account = $this->auth->cekPassword();
		if (!isset($account)) {
			$account = new \stdClass();
			$account->password = FALSE;
		}
		if (md5($this->input->post('password', TRUE)) == $account->password) {
			return TRUE;
		} else {
			echo "<script>alert('Username dan Password tidak sesuai')</script>";
			return FALSE;
		}
	}
}